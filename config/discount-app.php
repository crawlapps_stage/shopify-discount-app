<?php

return [
    'basic' => [
        "label" => "Default Rule - All products",
        "is_enable" => 1,
        "start_date" => [
            "publish_now" => true,
            "schedule_date" => null,
        ],
        "select_product" => 0,
        "last_sale" => [
            "is_day" => 1,
            "value" => "120",
        ],
        "set_discount" => [
            [
                "value" => "5",
                "is_dollar" => 0,
                "level" => "10",
                "error_class" => 0,
                "priority" => 1,
            ],
            [
                "value" => "10",
                "is_dollar" => 0,
                "level" => "20",
                "error_class" => 0,
                "priority" => 2,
            ],
            [
                "value" => "20",
                "is_dollar" => 0,
                "level" => "30",
                "error_class" => 0,
                "priority" => 3,
            ],
            [
                "value" => "30",
                "is_dollar" => 0,
                "level" => "40",
                "error_class" => 0,
                "priority" => 4,
            ],
        ],
        "stop_discount" => [
            "stock" => [
                "is_enable" => 1,
                "value" => "10",
            ],
            "date" => [
                "is_enable" => 0,
                "value" => null,
            ],
        ],
        "allow_if_out_ofstock" => 1,
        "type" => 0,
    ],
    'advance' => [
        "label" => "Default Rule - All products",
        "is_enable" => 1,
        "start_date" => [
            "publish_now" => true,
            "schedule_date" => null,
        ],
        "select_product" => 0,
        "last_sale" => [
            "is_day" => 1,
            "value" => "120",
        ],
        "set_discount" => [
            [
                "value" => "5",
                "is_dollar" => 0,
                "level" => "10",
                "error_class" => 0,
                "priority" => 1,
            ],
            [
                "value" => "10",
                "is_dollar" => 0,
                "level" => "20",
                "error_class" => 0,
                "priority" => 2,
            ],
            [
                "value" => "20",
                "is_dollar" => 0,
                "level" => "30",
                "error_class" => 0,
                "priority" => 3,
            ],
            [
                "value" => "30",
                "is_dollar" => 0,
                "level" => "40",
                "error_class" => 0,
                "priority" => 4,
            ],
        ],
        "stop_discount" => [
            "stock" => [
                "is_enable" => 1,
                "value" => "10",
            ],
            "date" => [
                "is_enable" => 0,
                "value" => null,
            ],
        ],
        "allow_if_out_ofstock" => 1,
        "type" => 0,
    ]
];
