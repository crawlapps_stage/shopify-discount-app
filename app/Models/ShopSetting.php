<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class ShopSetting extends Model
{
    use UsesUuid;
    public $incrementing = false;
    public $timestamps = false;
    protected $casts = [
        'tags' => 'array',
        'vendors' => 'array',
        'product_types' => 'array',
    ];
}
