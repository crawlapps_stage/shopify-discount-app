<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;
class RuleSet extends Model
{
    protected $table = 'rulesets';
    use UsesUuid;
    protected $fillable = ['shop_id', 'label', 'is_enable', 'start_date', 'select_product', 'last_sale', 'set_discount', 'stop_discount', 'allow_if_out_ofstock','type'];
    protected $casts = [
        'start_date' => 'array',
        'last_sale' => 'array',
        'set_discount' => 'array',
        'stop_discount' => 'array',
        'date' => 'array',
    ];

    public $incrementing = false;

    public function selected_product_items(){
        return $this->hasMany(SyncVariant::class,'ruleset_id','id');
    }
}
