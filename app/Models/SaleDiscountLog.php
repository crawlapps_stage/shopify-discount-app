<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class SaleDiscountLog extends Model
{
    use UsesUuid;
    public $incrementing = false;

    public function ruleset() {
        return $this->belongsTo(RuleSet::class, 'ruleset_id','id');
    }
}
