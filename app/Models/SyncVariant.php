<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class SyncVariant extends Model
{
    use UsesUuid;

    protected $fillable = ['shop_id', 'ruleset_id', 'product_id', 'variant_id', 'image', 'title', 'original_price', 'is_allocated','total_sale', 'current_level', 'current_stock', 'current_price', 'updated_at'];

    public $incrementing = false;
}
