<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use UsesUuid;
    protected $table = "product_variants";
    public $incrementing = false;
}
