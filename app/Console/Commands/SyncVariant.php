<?php

namespace App\Console\Commands;

use App\Jobs\VariantSyncJob;
use App\Models\Shop;
use Illuminate\Console\Command;
use App\Models\SyncVariant as ModelSyncVariant;

class SyncVariant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:variant {shop : 0} {--queue=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    /*  \Log::info($this->argument('shop'));
        \Log::info($this->option('queue'));*/
        VariantSyncJob::dispatch($this->argument('shop'));
    }



}
