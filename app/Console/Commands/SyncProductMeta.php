<?php

namespace App\Console\Commands;

use App\Models\Shop;
use App\Models\ShopSetting;
use Illuminate\Console\Command;

class SyncProductMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:productmeta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shops = Shop::get();
        foreach($shops as $key => $val) {
            $this->productMeta($val);
        }
    }

    public function productMeta($shop){

        $entity = ShopSetting::where('shop_id',$shop->id)->first();
        if(!$entity){
            $count = $shop->api()->rest('get', '/admin/variants/count.json');
            $entity = new ShopSetting;
            $entity->shop_id = $shop->id;
            $entity->total_variants = @$count->body->count;
            $entity->save();
        }

        $product_types = $vendors = $tags = [];
        $count = $shop->api()->rest('GET', '/admin/products/count.json');
        $totalPage = ceil($count->body->count / 250);
        for($i=1; $i<=$totalPage; $i++){
            $data250 = $shop->api()->rest('GET', '/admin/products.json',['fields'=>'tags,vendor,product_type','page'=>$i,'limit'=>250]);
            if(!$data250->errors){
                foreach (@$data250->body->products as $key => $value) {
                    $tags = array_merge($tags,explode(', ',$value->tags));
                    $vendors = array_merge($vendors,explode(', ',$value->vendor));
                    $product_types = array_merge($product_types,explode(', ',$value->product_type));
                }
            }else{
                /*\Log::info("-------------- error -------------------");
                \Log::info(json_encode($data250));*/
            }
        }

        $tags = array_values(array_unique(array_filter($tags)));
        $vendors = array_values(array_unique(array_filter($vendors)));
        $product_types = array_values(array_unique(array_filter($product_types)));


        $entity->tags = $tags;
        $entity->vendors = $vendors;
        $entity->product_types = $product_types;
        $entity->save();
    }
}
