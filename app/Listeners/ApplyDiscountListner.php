<?php

namespace App\Listeners;

use App\Models\RuleSet;
use App\Models\Shop;
use App\Models\SyncVariant as ModelSyncVariant;
use App\Events\DiscountEvent;
use App\Models\Product;
use App\Traits\Helper;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Carbon\Carbon;
class ApplyDiscountListner implements ShouldQueue
{

    public $days = 0;
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  DiscountEvent  $event
     * @return void
     */
    public function handle(DiscountEvent $event)
    {
        $type = @$event->type;
        if($type == 'single'){
            $this->checkVariantStockLevel($event->entity);
        }else{
            $ruleset = RuleSet::where('is_enable',1)->get();
            foreach ($ruleset as $key => $val){
                $status = $this->checkDiscountAbility($val);
                if($status){
                    $this->checkVariantStockLevel($val);
                }
            }
        }
    }

    // Condition 2
    public function checkVariantStockLevel($entity){
        \Log::info("--------- 5 ----------");
        $shop = Shop::find($entity->shop_id);
        $currentTimeStamp = Carbon::parse(time());
        // entity->type = 0 means this is a default ruleset which has all variants
        if($entity->type == 0){
            //$selected = Product::where('shop_id',$entity->shop_id)->get()->pluck('variant_id');
            $syncVariant = ModelSyncVariant::where('shop_id',$entity->shop_id)
                            ->whereNull('ruleset_id')
                            ->orderBy('updated_at')
                            ->get();
        }else{
            //$selected = $entity->selected_product_items->pluck('variant_id')->toArray();
            $syncVariant = ModelSyncVariant::where('shop_id',$entity->shop_id)
                ->where('ruleset_id',$entity->id)
                ->orderBy('updated_at')
                ->get();
        }
        \Log::info("--------- 6 ----------");
        $stop_discount_stock = $entity->stop_discount['stock']['is_enable'];
        $stop_discount_stock_value = $entity->stop_discount['stock']['value'];

        $plan_type = 0;
        foreach ($syncVariant as $key => $val){

            if($val->current_level >=4 || $plan_type){
                \Log::info("--------- 7 continue ----------");
                continue;
            }

            if($shop->plan_id == 1 ) {
                $plan_type = 1;
            }

            \Log::info("--------- 8 ----------");
            if($stop_discount_stock == 1 && $val->current_stock < $stop_discount_stock_value){
                \Log::info("--------- 9 continue ----------");
                if ($val->current_level > 0 ) {
                    \Log::info("--------- 10 False----------");
                    $parameters = [];
                    $parameters['price'] = $val->original_price;
                    $updateStatus = Helper::shopifyUpdateVariant($val->variant_id,$parameters,$shop);
                    if($updateStatus) {
                        $val->timestamps = false;
                        $val->ruleset_id = null;
                        $val->total_sale = 0;
                        $val->current_level = 0;
                        $val->save();
                    }
                }
                continue;
            }
            $star_time = Carbon::parse($val->updated_at->timestamp);
            $diff = $currentTimeStamp->diffInDays($star_time);

            \Log::info("--------- 11 continue with 8 ----------");

            $discount_levels = [];
            $dhisdaysHours  = $this->daysHours($entity);
            if($val->current_level == 0){
                $total_days = $dhisdaysHours['days'];
                $discount_levels = collect($entity->set_discount)->first();
            }else{
                $discount_levels = collect($entity->set_discount)->firstWhere('priority', $val->current_level);
                $total_days = $discount_levels['level'] + $dhisdaysHours['days'];
                $discount_levels = [];
                $discount_levels = collect($entity->set_discount)->firstWhere('priority', $val->current_level+1);
            }

            \Log::info("----- variant_id:". $val->variant_id);
            \Log::info("----- star_time:". $star_time);
            \Log::info("----- this->days:". $dhisdaysHours['days']);
            \Log::info("----- Total Days:". $total_days);
            \Log::info("----- diff:". $diff);
            //$total_days = $discount_levels['level'] + $entity->last_sale['value'];
            \Log::info("--------- 12 ----------");

            if($diff >= $total_days ) {
                \Log::info("--------- 13 ----------");
                // Apply discount.
                $parameters = [];
                //$variant = Helper::shopifyGetVariant($val->variant_id,$shop);
                if($discount_levels['is_dollar']){
                    $parameters['price'] = $val->current_price - $discount_levels['value'];
                }else{
                    $discount_price = ($val->current_price * $discount_levels['value']) / 100;
                    $parameters['price'] = $val->current_price - $discount_price;
                }
                \Log::info("--------- 15 ----------");
                \Log::info($parameters);
                $updateStatus = Helper::shopifyUpdateVariant($val->variant_id,$parameters,$shop);
                if($updateStatus) {
                    //\Log::info(json_encode($updateStatus));
                    \Log::info("--------- 16 ----------");
                    $val->timestamps = false;
                    //$val->total_sale = 0;
                    $val->current_price = $parameters['price'];
                    $val->current_level = $val->current_level+1;
                    $val->save(['timestamps' => false]);
                    \Log::info("-------------------- variant_id: ".$val->id);
                }
                \Log::info("--------- 17 ----------");
            }
            \Log::info("--------- 18 end for loop ----------");
        }
    }

    // Condition 1
    public function checkDiscountAbility($entity){
        \Log::info("--------- 1 ----------");
        //return true;
        if($entity->stop_discount['date']['is_enable'] == 1) {
            $star_time = strtotime($entity->stop_discount['date']['value']);
            if($star_time < time() ){
                $entity->is_enable = 0;
                $entity->save();
                $this->resetVariantPrice($entity);
            }
        }
        \Log::info("--------- 2 ----------");
        // check is enabled
        if($entity->is_enable == 0){
            return false;
        }
        \Log::info("--------- 3 ----------");
        $star_time = Carbon::parse($entity->created_at->timestamp);
        $end_time = Carbon::parse(time());

        // check the days or hours are passed
        $dhisdaysHours  = $this->daysHours($entity);
        \Log::info($dhisdaysHours['totalDuration'] ."<". $dhisdaysHours['check']);
        /*if($totalDuration < $check){
            $this->days = 0;
            return false;
        }*/
        \Log::info("--------- 4 ----------");

        // check it's a now or scheduled and it passed
        $schedule_date = ($entity->start_date['schedule_date'] != null)?strtotime($entity->start_date['schedule_date']):null;
        if(($schedule_date != null && $schedule_date <= time()) || $entity->start_date['publish_now'] == 1){
        }else{
            return false;
        }
        \Log::info("--------- 5 ----------");
        return true;
    }

    public function daysHours($entity){
        $star_time = Carbon::parse($entity->created_at->timestamp);
        $end_time = Carbon::parse(time());
        if($entity->last_sale['is_day'] == 0){
            $totalDuration = $end_time->diffInHours($star_time); //54
            $check = $entity->last_sale['value']; //5
            $this->days = ceil($check / 24);
        } else if($entity->last_sale['is_day'] == 1){
            $totalDuration = $end_time->diffInDays($star_time); //121
            $this->days = $check = $entity->last_sale['value']; //120
        }
        return ["totalDuration" =>$totalDuration, "check" => $check, 'days' => $this->days ];
    }
    // reset variants price
    public function resetVariantPrice($entity) {
        $shop = Shop::find($entity->shop_id);
        if($entity->type == 0){
            //$selected = Product::where('shop_id',$entity->shop_id)->get()->pluck('variant_id');
            $syncVariant = ModelSyncVariant::where('shop_id',$entity->shop_id)
                ->whereNull('ruleset_id')
                ->orderBy('updated_at')
                ->get();
        }else{
            //$selected = $entity->selected_product_items->pluck('variant_id')->toArray();
            $syncVariant = ModelSyncVariant::where('shop_id',$entity->shop_id)
                ->where('ruleset_id',$entity->id)
                ->orderBy('updated_at')
                ->get();
        }
        foreach ($syncVariant as $key => $val) {
            if ($val->current_level > 0) {
                $parameters = [];
                $parameters['price'] = $val->original_price;
                $updateStatus = Helper::shopifyUpdateVariant($val->variant_id, $parameters, $shop);
                if ($updateStatus) {
                    $val->timestamps = false;
                    $val->ruleset_id = null;
                    $val->total_sale = 0;
                    $val->current_level = 0;
                    $val->save();
                }
            }
        }
    }

    public function shouldQueue(DiscountEvent $event)
    {
        \Log::info('shouldQueue');
        $type = $event->type;
        if($type == 'single'){
            return $this->checkDiscountAbility($event->entity);
        }
        return true;
    }
}
