<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('checkzero', function($attribute, $value, $parameters, $validator) {
            $collection = collect($value)->firstWhere('value', '<=' ,0);
            if(!is_null($collection)){
                return false;
            }

            $last_collection = collect($value)->last();
            $collection = collect($value)->firstWhere('level', '<=' ,0);

            if($collection['priority'] == $last_collection['priority']){
                return true;
            }
            elseif(!is_null($collection)){
                return false;
            }
            return true;
        });
        Schema::defaultStringLength(191);
    }
}
