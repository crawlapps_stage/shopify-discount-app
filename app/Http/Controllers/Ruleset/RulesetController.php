<?php

namespace App\Http\Controllers\Ruleset;

use App\Console\Commands\SyncVariant;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\SaleDiscountLog;
use App\Models\ShopSetting;
use Illuminate\Http\Request;
use App\Models\RuleSet;
use App\Models\SyncVariant as ModelSyncVariant;
use App\Events\DiscountEvent;
use App\Http\Requests\RulesetRequest;

class RulesetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$entity = RuleSet::with('selected_product_items')->find('f818ecc3-09ed-4421-8af4-8bee0eb2d8a0');
        //event(new DiscountEvent($entity, 'single'));
        if ($request->has('api')) {
            return $this->apiIndex($request);
        }
    }

    public function discountedAPIIndex(Request $request)
    {
        $shop = \ShopifyApp::shop();
        $currency = $shop->api()->rest('GET','admin/shop.json',['fields' => 'currency'])->body->shop->currency;
        $currency = currency($currency)->getSymbol();
        $entities = SaleDiscountLog::with(['ruleset' => function($query){
            $query->select('label', 'id','created_at');
        }])->where('shop_id', $shop->id)->orderBy('created_at', 'desc')->get();
        return response(['entities' => $entities, 'currency' => $currency], 200);
    }

    public function apiIndex($request)
    {
        $shop = \ShopifyApp::shop();
        $entities = [];
        $show_sync_message = 0;
        if($request->api == 2){
            $show_sync_message = ShopSetting::where('shop_id',$shop->id)->first()->total_variants;
            $sync = \App\Models\SyncVariant::where('shop_id',$shop->id)->count();
            if($sync < $show_sync_message)
                $show_sync_message = 1;
            else
                $show_sync_message = 0;
        }else{
            $entities = RuleSet::where('shop_id', $shop->id)->orderBy('created_at', 'desc')->get();
        }
        return response(['entities' => $entities, 'show_sync_message' => $show_sync_message, 'plan' => $shop->plan_id], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RulesetRequest $request)
    {
        $entity = $request->all();
        unset($entity['selected_product_items']);
        $entity['shop_id'] = \ShopifyApp::shop()->id;
        $entity = RuleSet::create($entity);
        $this->addProducts($request, $entity);
        event(new DiscountEvent($entity, 'single'));
        return response(['message' => 'Successfully Created'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = RuleSet::with('selected_product_items')->find($id);
        return response($entity, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RulesetRequest $request, $id)
    {
        $entity = $request->all();
        unset($entity['selected_product_items']);
        $entity = RuleSet::updateOrCreate(['id' => $id], $entity);
        $this->addProducts($request, $entity, $id);
        event(new DiscountEvent($entity, 'single'));
        return response(['message' => 'Successfully Updated'], 200);
    }

    public function addProducts($request, $entity, $id = null)
    {
        if ($request->select_product && !is_null($request->selected_product_items)) {
            $variants = array_column($request->selected_product_items, 'variant_id');
            ModelSyncVariant::where('shop_id', $entity->shop_id)->whereIn('variant_id', $variants)->update(['ruleset_id'=>$entity->id, 'updated_at' => \DB::raw('updated_at')]);
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = \ShopifyApp::shop();
        $ruleset_product = ModelSyncVariant::where('shop_id', $shop->id)->where('ruleset_id',$id);
        $ruleset_product->update(['ruleset_id' => null,'total_sale' => 0, 'current_level' => 0,'current_price' => \DB::raw('original_price')]);
        $entity = RuleSet::find($id)->delete();
        return response(['data' => []],200);
    }

    public function check(Request $request)
    {
        $shop = \ShopifyApp::shop();
        $notfound_image = asset('images/not_found.png');
        $variant_ids = array_column($request['data'], 'variant_id');
        $entity = ModelSyncVariant::where('shop_id', $shop->id)->whereNull('ruleset_id')->whereIn('variant_id',$variant_ids)->get(['variant_id','product_id','title', 'image']);

        //dd($entity->toSql(), $entity->getBindings());
        $entity->map(function($query) use ($notfound_image){
            $query->image =  @($query->image != null) ? $query->image : $notfound_image;
            $query->__new =  1;
            return $query;
        })->toArray();
        return \Response::json([
            'products' => $entity,
            'total_selected' => count($variant_ids),
            'total_added' => count($entity),
        ], 200);
    }

    public function deleteProduct($id,Request $request)
    {
        $shop = \ShopifyApp::shop();
        $ruleset_product = ModelSyncVariant::where('shop_id', $shop->id)->where('variant_id',$id)->first();
        $ruleset_id = $ruleset_product->ruleset_id;
        $ruleset_product->update(['ruleset_id' => null,'total_sale' => 0, 'current_level' => 0,'current_price' => \DB::raw('original_price')]);
        $entity = ModelSyncVariant::where('ruleset_id', $ruleset_id)->select('title', 'ruleset_id', 'id', 'image', 'product_id','variant_id')->get()->toArray();
        return response(['data' => $entity],200);
    }

    public function search(Request $request){
        $shop = \ShopifyApp::shop();
        $entity = ShopSetting::where('shop_id',$shop->id)->select('tags','vendors','product_types')->first();
        return response($entity,200);
    }
}
