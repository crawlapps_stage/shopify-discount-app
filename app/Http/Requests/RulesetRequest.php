<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;

class RulesetRequest extends FormRequest
{
    public static $rules = [
        'label'  => 'required',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
        switch (Route::currentRouteName()) {
            case 'ruleset.store':
                {
                    $inputes = $this->all();
                    //dump($inputes);
                    if(!$inputes['start_date']['publish_now']){
                        $rules['start_date.schedule_date'] = "required|date";
                    }
                    //if($inputes['select_product'] && count($inputes['selected_product_items']) <= 0 ){
                    if($inputes['select_product']){
                        $rules['selected_product_items'] = "required|array|min:1";
                    }

                    $rules['last_sale.value'] = "required|gte:1";
                    $rules['set_discount'] = "required|checkzero";

                    if($inputes['stop_discount']['stock']['is_enable'] && (int)$inputes['stop_discount']['stock']['value'] <= 0 ){
                        $rules['stop_discount.stock.value'] = "required|numeric|min:1";
                    }

                    if($inputes['stop_discount']['date']['is_enable'] ){
                        $rules['stop_discount.date.value'] = "required|date";
                    }

                    return $rules;
                }
            case 'ruleset.update':
                {

                    return $rules;
                }
            default:
                break;
        }
        return [];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'label.required'  => 'The Ruleset name is required',
            'start_date.schedule_date.required'  => 'The on schedule date is required',
            'selected_product_items.required'  => 'Select atleast 1 product.',
            'last_sale.value.gte'  => 'Last product sale must be more than 0',
            'set_discount.checkzero'  => 'All the values of set discount must be more than 0',
            'stop_discount.stock.value.required'  => 'The stop discount on stock level must be more than 0',
            'stop_discount.date.value.required'  => 'The stop discount on date is required',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
