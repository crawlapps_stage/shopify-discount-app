<?php


namespace App\Traits;

trait Helper
{
    public static function getCollections($shop)
    {

        $condition = '{
                        collections(first: 250) {
                            edges {
                              node {
                                id
                                title
                              }
                            }
                          }
                        }';
        $collections = $shop->api()->graph($condition);
        $prepare = $data = [];
        if (@$collections->body->collections->edges) {
            foreach ($collections->body->collections->edges as $key => $val) {
                $node = $val->node;
                $prepare['label'] = $node->title;
                $prepare['value'] = str_replace("gid://shopify/Collection/", "", $node->id);
                $data[] = $prepare;
            }
        }
        return $data;
    }

    public static function createMetafields($shop,$parameters,$product_id)
    {
        $url = '/admin/products/' . $product_id . '/metafields.json';

        $metafield['metafield'] = $parameters;
        $resp = $shop->api()->rest('POST', $url, $metafield);


        if (@$resp->body->metafield) {
            return $resp->body->metafield;
        }

        return false;
    }

    public static function getProductMetafields($id, $shop)
    {
        $condition = '{
                            product(id:"gid://shopify/Product/' . $id . '")
                            {
                              metafields(first: 250) {
                                edges {
                                    node {
                                        legacyResourceId
                                        namespace
                                        key
                                        value
                                      }
                                    }
                                }
                            }
                        }';
        $product = $shop->api()->graph($condition);

        if (!$product->errors) {
            return @count($product->body->product) > 0 ? $product->body->product : [];
        }
        return false;
    }

    public static function shopifyUpdateVariant($id, $parameters, $shop){
        $url = '/admin/variants/' . $id . '.json';
        $metafield['variant'] = $parameters;
        $resp = $shop->api()->rest('PUT', $url, $metafield);
        if(!$resp->errors){
            return @count($resp->body->variant) > 0 ? $resp->body->variant : [];
        }
        return false;
    }

    public static function shopifyGetVariant($id, $shop){
        $url = '/admin/variants/' . $id . '.json';
        $resp = $shop->api()->rest('Get', $url);
        if(!$resp->errors){
            return @count($resp->body->variant) > 0 ? $resp->body->variant : [];
        }
        return false;
    }

}
