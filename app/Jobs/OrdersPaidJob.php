<?php namespace App\Jobs;

use App\Models\SaleDiscountLog;
use App\Models\Shop;
use App\Models\SyncVariant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OrdersPaidJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = Shop::where('shopify_domain', $this->shopDomain)->first();
        $order = json_encode($this->data);
        $order = json_decode($order);
        foreach ($order->line_items as $key => $val) {
            $entity = SyncVariant::where('shop_id',$shop->id)->where('product_id',$val->product_id)->where('variant_id',$val->variant_id)->first();
            $entity->total_sale =  0;
            $entity->current_level  = 0;
            $entity->current_price = $entity->original_price;
            $entity->save();

            $order = new SaleDiscountLog();
            $order->variant_id = $val->variant_id;
            $order->title = $entity->title;
            $order->original_price = $entity->original_price;
            $order->image = $entity->image;
            $order->discounted_price = $val->price;
            $order->level = $val->current_level;
            $order->ruleset_id = $entity->ruleset_id;
            $order->shop_id = $shop->id;
            $order->save();
        }
    }
}
