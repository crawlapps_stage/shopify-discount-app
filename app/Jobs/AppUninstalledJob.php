<?php

namespace App\Jobs;

use App\Models\RuleSet;
use App\Models\ShopSetting;
use App\Models\SyncVariant;

class AppUninstalledJob extends \OhMyBrew\ShopifyApp\Jobs\AppUninstalledJob
{

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        if (!$this->shop) {
            return false;
        }
        $this->deleteRecords();
        $this->cancelCharge();
        $this->cleanShop();
        $this->softDeleteShop();
        return true;
    }

    public function deleteRecords(){
        SyncVariant::where('shop_id',$this->shop->id)->delete();
        RuleSet::where('shop_id',$this->shop->id)->delete();
        ShopSetting::where('shop_id',$this->shop->id)->delete();
    }
}
