<?php

namespace App\Jobs;

use App\Models\RuleSet;
use App\Models\Shop;
use App\Models\ShopSetting;
use App\Models\SyncVariant as ModelSyncVariant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class VariantSyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $requestTimestamp;
    protected $leakRate = 50; // Leak rate per second
    protected $buffer = .1; // buffer seconds

    public $migrationId;

    public $scopeId;

    public $scopeType;

    public $sourceType;

    public $existingData;

    public $migration;
    public $is_single;

    public function __construct($is_single, $migrationId = null, $sourceType = null, $existingData = [])
    {
        $this->is_single = $is_single;
        $this->migrationId = $migrationId;
        $this->sourceType = $sourceType;
        $this->existingData = $existingData;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->is_single == 0) {
            $shops = Shop::get();
            foreach($shops as $key => $val) {
                $this->variantRecursive($val, 'productVariants', $limit = 250, $after = null, $existData = [], $call = 0, 'storeInDb');
            }
        }else
        {
            $shop = Shop::find($this->is_single);

            $data = $this->variantRecursive($shop, 'productVariants', $limit = 250, $after = null, $existData = [], $call = 0, 'storeInDb');

            $ruleset = RuleSet::where('shop_id', $shop->id)->where('type',0)->first();
            if(!$ruleset) {
                $entity = config('discount-app.basic');
                $entity['shop_id'] = $shop->id;
                $entity = RuleSet::create($entity);
            }
        }
    }

    public function storeInDb($shop, $products = []) {

        foreach ($products as $key => $val) {
            $node = $val->node;
            $entity = ModelSyncVariant::where('variant_id', $node->legacyResourceId)->where('shop_id',$shop->id)->first();
            if(!$entity) {
                $entity = new ModelSyncVariant();
                $entity->original_price = $node->price;
                $entity->shop_id = $shop->id;
            }
            $entity->product_id = $node->product->legacyResourceId;
            $entity->variant_id = $node->legacyResourceId;
            $entity->current_stock = $node->inventoryQuantity;
            $entity->current_price = $node->price;
            $entity->image = @$node->image->originalSrc;
            $entity->title = $node->product->title ."-". @$node->title;
            $entity->updated_at = date("Y-m-d h:i:s",strtotime($node->product->updatedAt));
            $entity->save(['timestamps' => false]);
        }
    }

    public function getAllProducts($count, $after = NULL, $shop)
    {
        $after = $after != '' ? 'after: "' . $after.'"' : '';
        $queryString = 'first: ' . $count . ',' . $after;
        \Log::info($queryString);
        $condition = '{
              productVariants(' . $queryString . ') {
                        pageInfo {
                          hasNextPage
                        }
                        edges {
                          cursor
                          node {
                            inventoryQuantity
                            legacyResourceId
                            title
                            image {
                              originalSrc
                            }
                            price
                            product {
                              updatedAt
                              legacyResourceId
                              title
                            }
                          }
                        }
                      }
                    }';
        $product = $shop->api()->graph($condition);
        \Log::info(json_encode($product));
        if (!$product->errors) {
            return @count($product->body->productVariants) > 0 ? $product->body->productVariants : false;
        }

        return false;
    }

    public function variants(Shop $shop, $scope, $limit = 250, $after = null)
    {
        $query = 'query($limit: Int! $after: String){
              ' . $scope . '(first: $limit after: $after) {
                pageInfo{
                  hasNextPage
                  hasPreviousPage
                }
                edges{
                  cursor
                  node{
                    inventoryQuantity
                    legacyResourceId
                    title
                    image {
                      originalSrc
                    }
                    price
                    product {
                      updatedAt
                      legacyResourceId
                      title
                    }
                  }
                }
              }
            }';
        $parameters = ['limit' => $limit];
        if ($after) {
            $parameters['after'] = $after;
        }
        $data = $this->graph($shop, $query, $parameters);
        return $data;
    }

    protected function graph(Shop $shop, string $query, array $parameters = [])
    {
        $queryCost = $this->calculateQueryCost($query, $parameters);
        $availableCost = $shop->api()->getApiCalls('graph', 'left');

        $availableCostSince = $this->requestTimestamp;
        $extraSecondsSince = round(microtime(true) - $availableCostSince, 3);
        $availableCost += ($extraSecondsSince * $this->leakRate);
        \Log::info("---------------------------");
        \Log::info($queryCost." > ".$availableCost);
        if ($queryCost > $availableCost) {
            $requireExtraCost = $queryCost - $availableCost;
            $awaitSeconds = $requireExtraCost / $this->leakRate;
            \Log::info("sleep: ". ceil($awaitSeconds));
            sleep(ceil($awaitSeconds));
        }
        $data = $shop->api()->graph($query, $parameters);
        $this->requestTimestamp = end($data->timestamps);
        return $data;
    }

    protected function calculateQueryCost(string $query, array $variable = [])
    {
        try {
            $initialCost = 2;
            $queryArray = $this->parseGraphQLtoArray($query, $variable);
            $firstLimit = null;
            $isFirst = true;
            $queryCost = $initialCost; // Added Initial Cost
            foreach ($queryArray as $key => $value) {
                if ($firstLimit == null) {
                    preg_match('/first:(.*)\d/', $key, $firstLimit);
                    $firstLimit = (int)str_replace('first:', '', $firstLimit[0]);
                }
                if (is_array($value) && !isset($value['edges']['node'])) {
                    $queryCost += $firstLimit; // Added any operational
                }
                if (is_array($value) && isset($value['edges']['node'])) {
                    $subArray = $value['edges']['node'];
                    $queryCost += $this->calculateNodes($subArray, $key, $isFirst, $firstLimit, $initialCost); // Child Nodes
                }
                if ($isFirst) {
                    $isFirst = false;
                }
            }
            $queryCost += $firstLimit; // Added First Limit
            return $queryCost;
        } catch (\Exception $exception) {
            return 0;
        }
    }

    protected function parseGraphQLtoArray($query, array $variables = [])
    {
        $query = preg_replace('/^[query](.*?)\)/', '', $query, 1);
        if (count($variables)) {
            foreach ($variables as $key => $variable) {
                $query = preg_replace('/\$' . $key . '/', $variable, $query);
            }
        }
        $query = str_replace('"', '\"', $query);
        $query = str_replace("{", "{\n", $query);
        $query = str_replace("}", "\n}\n", $query);
        $query = array_map("trim", explode("\n", $query));
        foreach ($query as $k => $line) {
            // strip comments
            $line = explode("#", $line);
            $line = $line[0];
            // skip opening or closing tags
            if ($line === "{" || $line === "") {
                continue;
            }
            // declare as object value
            if (strpos($line, "{") !== false) {
                $name = trim(str_replace("{", "", $line));
                $query[$k] = '"' . $name . '": {';
                continue;
            }
            if (strpos($line, "}") !== false) {
                $query[$k] .= ',';
                continue;
            }
            $query[$k] = '"' . $line . '": true,';
        }
        $query = implode("", $query);
        // cut last comma
        $query = substr($query, 0, -1);
        // cut trailing commas
        $query = str_replace(",}", "}", $query);
        // produce php array
        $retval = json_decode($query, true);
        if (is_null($retval)) {
            throw new \Exception(sprintf("Error when parsing GraphQL fields: '%s'", $query));
        }
        return $retval;
    }

    protected function calculateNodes(array $array, $arrayKey, $isFirst, $firstLimit, $initialCost, $queryCost = 0)
    {
        if (!$isFirst) {
            preg_match('/first:(.*)\d/', $arrayKey, $limit);
            $limit = (int)str_replace('first:', '', $limit[0]);
            $queryCost += ($initialCost * $firstLimit + ($firstLimit * $limit));
        }
        foreach ($array as $key => $value) {
            if (is_array($value) && !isset($value['edges']['node'])) {
                $queryCost += $firstLimit;
            }
            if (is_array($value) && isset($value['edges']['node'])) {
                $subArray = $value['edges']['node'];
                return $this->calculateNodes($subArray, $key, false, $firstLimit, $initialCost, $queryCost);
            }
        }

        return $queryCost;
    }

    public function variantRecursive(Shop $shop, $scope, $limit = 250, $after = null, $existData = [], $call = 0, $functionName = null)
    {
        $data = $this->variants($shop, $scope, $limit, $after);
        $checkNext = $data->body->$scope->pageInfo->hasNextPage ?? false;
        if (!isset($data->body->$scope) && isset($data->errors) && $data->errors == true) {
            // Just in case if body returns null instead of data
            \Log::info(json_encode($data));
            throw new \Exception($data->body);
        }
        if (isset($data->body->$scope->edges)) {
            $after = end($data->body->$scope->edges)->cursor ?? null;
            $responseData = [];
            foreach ($data->body->$scope->edges as $node) {
                $responseData[] = $node;
                $this->$functionName($shop, $responseData);
            }
            //$existData = array_merge($existData, $responseData);
        }
        if ($checkNext) {
            $call++;
            return $this->variantRecursive($shop, $scope, $limit, $after, $existData, $call, $functionName);
        }
        \Log::info("-----------------END--------------------");
        //\Log::info(count($existData));
        //return $existData;
        return true;
    }

}
