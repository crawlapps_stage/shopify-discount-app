<?php

namespace App\Jobs;

use App\Events\DiscountEvent;
use App\Models\RuleSet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Artisan::queue('sync:productmeta');

        \Artisan::queue('sync:variant', [
            'shop' => \ShopifyApp::shop()->id,
            '--queue' => 'true'
        ]);

        /*$shop = \ShopifyApp::shop();
        $ruleset = RuleSet::where('shop_id', $shop->id)->where('type',0)->first();

        if(!$ruleset) {
            $entity = config('discount-app.basic');
            $entity['shop_id'] = $shop->id;
            $entity = RuleSet::create($entity);
        }*/

        /*if (!$this->shop->isGrandfathered()) {
            $planName = $this->shop->api()->rest('GET', '/admin/shop.json')->body->shop->plan_name;
            if ($planName === 'affiliate' || $planName === 'staff_business') {
                $this->shop->grandfathered = true;
                $this->shop->save();
            }
        }*/
    }
}
