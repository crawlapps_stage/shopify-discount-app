<?php namespace App\Jobs;

use App\Models\Shop;
use App\Models\SyncVariant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductsCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = Shop::where('shopify_domain', $this->shopDomain)->first();
        $product = json_encode($this->data);
        $product = json_decode($product);

        foreach ($product->variants as $key => $val) {
            /*$entity = SyncVariant::where('shop_id',$shop->id)->where('product_id',$val->product_id)->where('variant_id',$val->variant_id)->first();
            if(!$entity) {*/
            $images = [];
            $images['images'] = ($product->images)?$product->images:[];
            $images = collect($images)->groupBy('variant_ids', true)->toArray();
            if(array_key_exists($val->id,$images)){
                $images = $images[$val->id]['images']['src'];
            }else{
                $images = null;
            }
                $entity = new SyncVariant;
                $entity->shop_id = $shop->id;
                $entity->variant_id = $val->id;
                $entity->product_id = $val->product_id;
                $entity->original_price = $val->price;
                $entity->image = $images;
                $entity->current_stock = $val->inventory_quantity;
                $entity->current_price = $val->price;
            //}
            $entity->title = $product->title ." - ". $val->title;
            $entity->save();
        }
    }
}
