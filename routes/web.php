<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shop','billable'])->name('home');

/*Route::get('/{path}', function () {
    return view('layouts.app');
})->middleware(['auth.shop']);*/

Route::group(['middleware' => ['auth.shop'] ], function () {
    Route::get('search','Ruleset\RulesetController@search');
    Route::get('discounted','Ruleset\RulesetController@discountedAPIIndex');
    Route::get('ruleset/delete-product/{id}','Ruleset\RulesetController@deleteProduct');
    Route::resource('ruleset','Ruleset\RulesetController');
    Route::post('ruleset/check','Ruleset\RulesetController@check');
});


Route::get('flush', function(){
    request()->session()->flush();
});

Route::get('test', function(){

        $d = ["images" => [
            "id" => 539438707724640965,
            "product_id" => 788032119674292922,
            "position" => 0,
            "created_at" => null,
            "updated_at" => null,
            "alt" => null,
            "width" => 323,
            "height" => 434,
            "src" => "\/\/cdn.shopify.com\/s\/assets\/shopify_shirt-39bb555874ecaeed0a1170417d58bbcf792f7ceb56acfe758384f788710ba635.png",
            "variant_ids" => [
                "788032119674292921",
                "788032119674292922",
                "788032119674292923",
                "788032119674292924",
            ]
        ],
            [
                "id" => 539438707724640962,
                "product_id" => 788032119674292922,
                "position" => 0,
                "created_at" => null,
                "updated_at" => null,
                "alt" => null,
                "width" => 323,
                "height" => 434,
                "src" => "\/\/cdn.shopify.com\/s\/assets\/shopify_shirt-39bb555874ecaeed0a1170417d58bbcf792f7ceb56acfe758384f788710ba639.png",
                "variant_ids" => [
                    "788032119674292925",
                    "788032119674292926",
                    "788032119674292927",
                    "788032119674292928",
                ]
            ]
        ];

        $e = collect($d)->groupBy('variant_ids',true);
        dd($e->toArray());

});
