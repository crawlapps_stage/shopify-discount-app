import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/List').default,
        name:'list',
        meta: {
            title: 'Current Rule List',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/ruleset/:id?',
        component: require('../components/pages/CreateEdit').default,
        name:'ruleset',
        meta: {
            title: 'New Ruleset',
            title_edit: 'Edit Ruleset',
            title_new: 'New Ruleset',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        }
    },
    {
        path:'/discounted',
        component: require('../components/pages/DiscountedTab').default,
        name:'discounted',
        meta: {
            title: 'Discounted Products',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        }
    }
];


// This callback runs before every route change, including on page load.


const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
