<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->insert(
            [
                [
                    'type' => 1,
                    'name' => 'Basic',
                    'price' => 10.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Basic Plan',
                    'trial_days' => 7,
                    'test' => 1,
                    'on_install' => 1
                ],
                [
                    'type' => 1,
                    'name' => 'Premium',
                    'price' => 15.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Premium Plan',
                    'trial_days' => 0,
                    'test' => 0,
                    'on_install' => 0,
                ]
            ]
        );
    }
}
