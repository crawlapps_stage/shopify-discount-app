<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRuleSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rulesets', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->string('label', 191)->nullable();
            $table->boolean('is_enable')->default(1)->comment('0: No, 1: Yes');
            $table->timestamp('start_date')->nullable();
            $table->tinyInteger('select_product')->nullable()->comment('all:0, selected:1');
            $table->json('last_sale')->nullable()->comment('{is_day:0/1[hour:0,day:1], value:0}');
            $table->json('set_discount')->nullable();
            $table->json('stop_discount')->nullable();
            $table->boolean('allow_if_out_ofstock')->nullable();
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rule_sets');
    }
}
