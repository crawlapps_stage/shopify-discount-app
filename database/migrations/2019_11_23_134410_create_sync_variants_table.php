<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_variants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->string('product_id')->nullable();
            $table->string('variant_id')->nullable();
            $table->string('image')->nullable();
            $table->string('title')->nullable();
            $table->string('original_price')->nullable();
            $table->boolean('is_allocated')->default(0);
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_variants');
    }
}
