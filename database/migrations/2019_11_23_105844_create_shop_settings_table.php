<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_settings', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('shop_id')->unsigned()->nullable();
            $table->json('tags')->nullable();
            $table->json('vendors')->nullable();
            $table->json('product_types')->nullable();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_settings');
    }
}
