<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCurrentLevelToSyncVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sync_variants', function (Blueprint $table) {
            $table->smallInteger('current_level')->default(0)->after('total_sale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sync_variants', function (Blueprint $table) {
            $table->dropColumn('current_level');
        });
    }
}
