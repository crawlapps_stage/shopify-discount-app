<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
        Schema::create('product_variants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('shop_id')->unsigned();
            $table->char('ruleset_id',36)->nullable();
            $table->string('product_id')->nullable();
            $table->string('variant_id')->nullable();

            $table->string('title')->nullable();
            $table->string('image')->nullable();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('ruleset_id')->references('id')->on('rulesets')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
