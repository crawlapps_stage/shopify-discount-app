<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToSyncVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sync_variants', function (Blueprint $table) {
            $table->string('current_price')->default(0)->after('current_level');
            $table->integer('current_stock')->default(0)->after('current_level');
            $table->char('ruleset_id', 36)->nullable()->after('shop_id');
            $table->foreign('ruleset_id')->references('id')->on('rulesets')->onDelete('CASCADE')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sync_variants', function (Blueprint $table) {
            $table->dropColumn('current_price');
            $table->dropColumn('current_stock');
        });
    }
}
